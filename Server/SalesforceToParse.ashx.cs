﻿using Parse;
using RedButton.Server.SFClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace RedButton.Server
{
    /// <summary>
    /// Summary description for SalesforceToParse
    /// </summary>
    public class SalesforceToParse : IHttpHandler
    {
        private IEnumerable<ParseUser> queryParse;


        public async void ProcessRequest(HttpContext context)
        {
            
            ParseClient.Initialize("6xsxHmWNjN2FCi5cD8cimFmfPHZMWmEt4oWzaenH", "q1bHziPGtrGMV6vNYM0gQfg3INqmdUlihSVDug9M");
            string parseID = context.Request["parseID"] ?? string.Empty;

            try
            {
                var soapClient = ResourceAccessHelper.SoapClient;
                var sessionHeader = ResourceAccessHelper.SessionHeader;
                var mruHeader = new MruHeader { updateMru = true };
                var packageVersions = new PackageVersion[0];
                var query = "SELECT Type,Reason FROM Case c Where c.userID__c = '" + parseID + "' ORDER BY CaseNumber";
                QueryResult result;
                soapClient.query(sessionHeader, new QueryOptions(), mruHeader, packageVersions, query, out result);
                int distressCounter = 0;
                int violentContentCounter = 0;
                if (result.records != null)
                {
                    foreach (object record in result.records)
                    {
                        if (((Case)(record)).Reason != null)
                        {
                            if (((Case)(record)).Reason.Contains("נשלחה פנייה למשטרה") || ((Case)(record)).Reason.Contains("הדיווח טופל בהצלחה"))
                            {
                                if (((Case)(record)).Type != null && ((Case)(record)).Type.Contains("איום באובדנות / מצוקה"))
                                {
                                    distressCounter++;
                                }
                                else
                                {
                                    violentContentCounter++;
                                }
                            }
                        }

                    }
                }
                await updateParseDB(parseID, distressCounter, violentContentCounter);
                List<int> reportCounters = new List<int>() { distressCounter, violentContentCounter };
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string txt = javaScriptSerializer.Serialize(reportCounters);
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(txt);
            }
            catch (Exception)
            {
                context.Response.ContentType = "application/json; charset=utf-8";
                context.Response.Write(parseID);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private async System.Threading.Tasks.Task<string> updateParseDB(string username, int distressCounter, int violentContentCounter)
        {
            var query = ParseObject.GetQuery("userReports")
                            .WhereEqualTo("userName", username);
            IEnumerable<ParseObject> results = await query.FindAsync();
            List<ParseObject> resultsList = results.ToList();
            ParseObject userReports = null;
            if (resultsList.Count > 0)
            {
                userReports = results.ToList()[0];
            }
            else
            {
                userReports = new ParseObject("userReports");
            }
            userReports["userName"] = username;
            userReports["distressCounter"] = distressCounter;
            userReports["violentContentCounter"] = violentContentCounter;
            await userReports.SaveAsync();
            return string.Empty;
        } 
    }
}