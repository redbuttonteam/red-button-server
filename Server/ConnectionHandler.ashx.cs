﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace RedButton.Server
{
    /// <summary>
    /// Summary description for ConnectionHandler
    /// </summary>
    public class ConnectionHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string customer = context.Request["customer"];
            if (customer.ToLower().Equals("noc"))
            {
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string txt = javaScriptSerializer.Serialize("The customer: " + customer + " is now connected");
                context.Response.ContentType = "text/html";
                context.Response.Write(txt);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}