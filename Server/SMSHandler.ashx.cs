﻿using MySql.Data.MySqlClient;
using RedButton.Server.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;

namespace RedButton.Server
{
    /// <summary>
    /// Summary description for SMSHandler
    /// </summary>
    public class SMSHandler : IHttpHandler
    {
        MySqlConnection cn;
        MySqlDataAdapter da;
        DataSet ds;
        MySqlCommandBuilder cmd;
        public void ProcessRequest(HttpContext context)
        {
            cn = new MySqlConnection("Database=redbutton; Data Source=68.178.143.146;UserId=redbutton;Password='Developer1!'");
            cn.Open();
            ds = new DataSet();
            da = new MySqlDataAdapter("select * from bttnUsers", cn);
            da.Fill(ds);
            ds.Tables[0].PrimaryKey = new DataColumn[] { ds.Tables[0].Columns[0] };
            DataRow row = ds.Tables[0].Rows.Find(context.Request["bttnID"]);
            int index = ds.Tables[0].Rows.IndexOf(row);
            if (index > -1)
            {
                SMSHelper.Send("0542467463", row.ItemArray[0].ToString(), row.ItemArray[1].ToString());
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}