﻿#region

using System;
using System.Net;
using System.Web;
using RedButton.Server.SFClient;
using System.Web.Script.Serialization;
using System.Web.Configuration;
using System.Threading;
using System.Collections.Generic;
using RedButton.Server.Helpers;
using RedButton.Server.Entities;
using System.Text;

#endregion

namespace RedButton.Server
{
    public class ReportHandler : IHttpHandler
    {
        private const string ContactID = "003b0000008NAb4";
        private const string AttachmentContentType = "image/png";
        private const string AttachmentName = "Screenshot";
        private static ErrorHelper errorHelper;
        private static RequestReport requestReport;

        #region IHttpHandler Members

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                errorHelper = ErrorHelper.Instance;
                requestReport = new RequestReport();

                try
                {
                    ResourceAccessHelper.ConnectToSalesForce();
                }
                catch (Exception ex)
                {
                    if (errorHelper == null)
                    {
                        errorHelper = ErrorHelper.Instance;
                    }
                    errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1001);
                    return;
                }

                //TempUpdate();

                //string userComment;
                //string code;
                //Uri uri;
                //byte[] attachmentBody;
                //string phoneNumber;
                //string nameInfo;
                //string contentSource;
                //string eventType;
                //string userID;
                //string timeUpload;
                //string pageName;
                //string appName;
                try
                {
                    //HandleParameters(context, out userComment, out uri, out attachmentBody, out code,
                    //    out phoneNumber, out nameInfo, out contentSource, out eventType, out userID,
                    //    out timeUpload, out pageName, out appName);
                    HandleParameters(context, requestReport);
                }
                catch (Exception ex)
                {
                    context.Response.StatusCode = 500;
                    context.Response.Write(ex.Message);
                    return;
                }

                // Get IP
                IPAddress addr = IPAddress.None;
                if (context.Request.UserHostAddress != null && !IPAddress.TryParse(context.Request.UserHostAddress, out addr))
                {
                    addr = IPAddress.None;
                }

                // Create case
                string caseID = string.Empty;
                try
                {
                    //caseID = CreateCase(userComment, addr, uri, code, phoneNumber, nameInfo,
                    //    contentSource, eventType, userID, timeUpload, pageName, appName);
                    caseID = CreateCase(requestReport, addr);
                }
                catch (Exception)
                {
                    if (errorHelper == null)
                    {
                        errorHelper = ErrorHelper.Instance;
                    }
                    errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1004);
                    return;
                }
                // Create attachment for case
                if (!string.IsNullOrEmpty(caseID))
                {
                    try
                    {
                        //CreateAttachment(caseID, attachmentBody);
                        CreateAttachment(caseID, requestReport.attachmentBody);
                    }
                    catch (Exception)
                    {
                        if (errorHelper == null)
                        {
                            errorHelper = ErrorHelper.Instance;
                        }
                        errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1005);
                        return;
                    }
                }

                var caseNumber = GetCaseNumber(caseID);
                if (requestReport.isSuicidalThreat)
                {
                    StringBuilder phoneList = new StringBuilder();
                    phoneList.Append("0507578738;");
                    phoneList.Append("0524235972;");
                    phoneList.Append("0506987544;");
                    phoneList.Append("0547848653;");
                    phoneList.Append("0545502996");
                    try
                    {
                        SMSHelper.Send(phoneList.ToString(), caseNumber, requestReport.userComment);
                    }
                    catch (Exception e)
                    {
                        errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1011);
                    }
                }

                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string txt = javaScriptSerializer.Serialize(caseNumber);
                context.Response.ContentType = "text/html";
                context.Response.Write(txt);
            }
            catch (Exception)
            {
                if (errorHelper == null)
                {
                    errorHelper = ErrorHelper.Instance;
                }
                errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1006);
                return;
            }
        }

        /// <summary>parameters in request: comment, uri, screenCaptureBase64png</summary>
        /// <returns>true if successful, false if not</returns>
        //private static void HandleParameters(HttpContext context, out string userComment, out Uri uri, out byte[] attachmentBody, out string code,
        //    out string phoneNumber, out string nameInfo, out string contentSource, out string eventType, out string userID,
        //    out string timeUpload, out string pageName, out string appName)
        private static void HandleParameters(HttpContext context, RequestReport rp)
        {
            rp.phoneNumber = context.Request["phoneNumber"] ?? string.Empty;
            rp.nameInfo = context.Request["nameInfo"] ?? string.Empty;
            rp.contentSource = context.Request["contentSource"] ?? string.Empty;
            rp.eventType = context.Request["eventType"] ?? string.Empty;
            rp.timeUpload = context.Request["timeUpload"] ?? string.Empty;
            rp.pageName = context.Request["pageName"] ?? string.Empty;
            rp.appName = context.Request["appName"] ?? string.Empty;
            rp.userID = context.Request["userID"] ?? string.Empty;
            rp.distressCaseDescription = context.Request["distressCaseDescription"] ?? string.Empty;

            switch (context.Request["isSuicidalThreat"])
            {
                case "true":
                    rp.isSuicidalThreat = bool.Parse("true");
                    break;
                case "false":
                    rp.isSuicidalThreat = bool.Parse("false");
                    break;
                default:
                    rp.isSuicidalThreat = bool.Parse("false");
                    break;
            }

            rp.uri = null;
            rp.attachmentBody = null;

            // comment
            rp.userComment = context.Request["comment"] ?? string.Empty;

            rp.code = context.Request["code"] ?? string.Empty;

            rp.code = requestReport.code == string.Empty ?
                WebConfigurationManager.AppSettings["Default_Case_Password"] : requestReport.code;

            // url
            string uriStr = context.Request["uri"];
            if (!string.IsNullOrEmpty(uriStr))
            {
                try
                {
                    requestReport.uri = new Uri(uriStr);
                }
                catch (Exception)
                {
                    if (errorHelper == null)
                    {
                        errorHelper = ErrorHelper.Instance;
                    }
                    errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1002);
                    return;
                }
            }

            // screenshot
            string captureBase64PNG = context.Request["screenCaptureBase64png"];
            if (string.IsNullOrEmpty(captureBase64PNG))
            {
                if (errorHelper == null)
                {
                    errorHelper = ErrorHelper.Instance;
                }
                errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1003);
                return;
            }
            try
            {
                captureBase64PNG = captureBase64PNG.Replace("data:image/png;base64,", "");
                requestReport.attachmentBody = Convert.FromBase64String(captureBase64PNG);
            }
            catch (Exception e)
            {
                errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1010);
            }
        }

        /// <returns>newly created case-ID</returns>
        //private string CreateCase(string userComment, IPAddress ip, Uri uri, string code, 
        //    string phoneNumber, string nameInfo, string contentSource, string eventType, string userID,
        //    string timeUpload, string pageName, string appName)
        private string CreateCase(RequestReport rp, IPAddress ip)
        {
            string urlStr = string.Empty;
            if (rp.uri != null)
            {
                urlStr = rp.uri.ToString();
                if (urlStr.Length > 250)
                {
                    urlStr = urlStr.Substring(0, 197);
                    urlStr = urlStr + "...";
                }
            }

            DateTime timeToConvert = DateTime.Now;
            TimeZoneInfo israelTime = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
            DateTime targetTime = TimeZoneInfo.ConvertTime(timeToConvert, israelTime);
            string time = targetTime.ToString("dd/MM/yy HH:mm:ss");
            Case c = new Case
            {
                Subject = string.IsNullOrEmpty(urlStr) ? string.Format("Date: {0}", time) : string.Format("URL: {0}, Date: {1}", urlStr, time),
                ContactId = ContactID,
                userDescription__c = rp.userComment,
                IP__c = ip.ToString(),
                URL__c = urlStr,
                ReferenceViewCode__c = rp.code,
                Domain__c = new Uri(urlStr).Host,
                PhoneNumber__c = rp.phoneNumber,
                ReportName__c = rp.nameInfo,
                ContentSource__c = rp.contentSource,
                EventType__c = rp.eventType,
                userID__c = rp.userID,
                TimeUpload__c = rp.timeUpload,
                PageName__c = rp.pageName,
                Origin = rp.appName,
                isSuicidalThreat__c = rp.isSuicidalThreat,
                isSuicidalThreat__cSpecified = rp.isSuicidalThreat,
                distressCaseDescription__c = rp.distressCaseDescription,
            };
            SaveResult[] res = ResourceAccessHelper.CreateRequest(new sObject[] { c });
            return res[0].id;
        }

        //Getting the the case number of a specific id.
        private string GetCaseNumber(string id)
        {
            var soapClient = ResourceAccessHelper.SoapClient;
            var sessionHeader = ResourceAccessHelper.SessionHeader;
            var mruHeader = new MruHeader { updateMru = true };
            var packageVersions = new PackageVersion[0];

            var query = "SELECT CaseNumber FROM Case c Where c.Id = '" + id + "' ORDER BY CaseNumber";
            QueryResult result;
            soapClient.query(sessionHeader, new QueryOptions(), mruHeader, packageVersions, query, out result);

            return ((Case)(result.records[0])).CaseNumber;
        }

        /// <returns>newly created attachment-ID</returns>
        private string CreateAttachment(string caseID, byte[] attachmentBody)
        {
            Attachment a = new Attachment { ContentType = AttachmentContentType, Body = attachmentBody, ParentId = caseID, Name = AttachmentName };
            SaveResult[] res = ResourceAccessHelper.CreateRequest(new sObject[] { a });
            return res[0].id;
        }

        #endregion
    }
}