﻿using System;
using System.IO;
using System.Web;
using System.Web.Script.Serialization;
using RedButton.Server.Entities;
using RedButton.Server.SFClient;

namespace RedButton.Server
{
    public class StatusHandler : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                ResourceAccessHelper.ConnectToSalesForce();

                string caseNumber;
                string code;
                HandleParameters(context, out caseNumber, out code);

                var caseStatus = GetCase(caseNumber, code);

                context.Response.Write(string.Format("{0}({1});", context.Request["callback"], new JavaScriptSerializer().Serialize(caseStatus)));
                context.Response.ContentType = "application/json";
            }
            catch (Exception ex)
            {
                //context.Response.Write(string.Format("{0}({1});", context.Request["callback"], new JavaScriptSerializer().Serialize("error")));
                context.Response.Write(string.Format("{0}({1});", context.Request["callback"], new JavaScriptSerializer().Serialize(ex.Message)));
                context.Response.ContentType = "application/json";
            }
        }

        private static void HandleParameters(HttpContext context, out string caseNumber, out string code)
        {
            caseNumber = context.Request["caseId"];
            code = context.Request["code"];
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private RequestStatus GetCase(string caseNumber, string code)
        {
            var soapClient = ResourceAccessHelper.SoapClient;
            var sessionHeader = ResourceAccessHelper.SessionHeader;
            var mruHeader = new MruHeader { updateMru = true };
            var packageVersions = new PackageVersion[0];

            var statusQuery = "SELECT CaseNumber,CreatedDate, Status, PublicCaseStatus__c,URL__c, Name__c, Field1__c, Reason, (SELECT Id FROM Attachments)  FROM Case " +
                              "WHERE CaseNumber = '" + caseNumber + "' and ReferenceViewCode__c = '" + code + "'";

            QueryResult result;

            soapClient.query(sessionHeader, new QueryOptions(), mruHeader, packageVersions, statusQuery, out result);
            var singleCase = (Case)result.records[0];

            var imageQuery = "SELECT Body, BodyLength, ContentType FROM Attachment where Id='" + ((Attachment)singleCase.Attachments.records[0]).Id + "'";

            soapClient.query(sessionHeader, new QueryOptions(), mruHeader, packageVersions, imageQuery, out result);
            var imageInfo = (Attachment)result.records[0];

            var pubStatus = singleCase.Status.Equals("טופל בהצלחה / הוסר") || singleCase.Status.Equals("נסגר") || singleCase.Status.Equals("נסגרה הבקשה") ? 
                singleCase.Reason : singleCase.PublicCaseStatus__c;
           
            var systemMessage = string.Empty;
            if(singleCase.Name__c != null){
                if (singleCase.Field1__c.Value)
                {
                    systemMessage = singleCase.Name__c;
                }
            }

            DateTime timeToConvert = singleCase.CreatedDate.Value;
            TimeZoneInfo israelTime = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
            DateTime targetTime = TimeZoneInfo.ConvertTime(timeToConvert, israelTime);
            string date = targetTime.ToString("dd/MM/yy HH:mm:ss");

            var status = new RequestStatus
            {
                Date = date,
                CaseNumber = singleCase.CaseNumber,
                Status = singleCase.Status,
                Body = Convert.ToBase64String(imageInfo.Body),
                BodyLength = imageInfo.BodyLength,
                ContentType = imageInfo.ContentType,
                Url = singleCase.URL__c,
                //PubStatus = singleCase.PublicCaseStatus__c
                PubStatus = pubStatus,
                SystemMessage = systemMessage
            };

            return status;
        }
    }
}