﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
using System.Data;
using System.Web.Script.Serialization;

namespace RedButton.Server
{
    /// <summary>
    /// Gets requests from child's phone to change location.
    /// </summary>
    public class LocationHandler : IHttpHandler
    {
        MySqlConnection cn;
        MySqlDataAdapter da;
        DataSet ds;
        MySqlCommandBuilder cmd;
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                cn = new MySqlConnection("Database=redbutton; Data Source=68.178.143.146;UserId=redbutton;Password='Developer1!'");
                cn.Open();
                ds = new DataSet();
                da = new MySqlDataAdapter("select * from bttnUsers", cn);
                da.Fill(ds);
                ds.Tables[0].PrimaryKey = new DataColumn[] { ds.Tables[0].Columns[0] };
                DataRow row = ds.Tables[0].Rows.Find(context.Request["bttnID"]);
                int index = ds.Tables[0].Rows.IndexOf(row);
                if (index > -1)
                {
                    ds.Tables[0].Rows[index].ItemArray = new object[] { context.Request["bttnID"], context.Request["location"] };
                }
                else
                {
                    ds.Tables[0].Rows.Add(context.Request["bttnID"], context.Request["location"]);
                }
                cmd = new MySqlCommandBuilder(da);
                da.InsertCommand = cmd.GetInsertCommand();
                da.DeleteCommand = cmd.GetDeleteCommand();
                da.UpdateCommand = cmd.GetUpdateCommand();
                da.Update(ds);
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string txt = javaScriptSerializer.Serialize("Succeded");
                context.Response.ContentType = "text/html";
                context.Response.Write(txt);
            }
            catch (Exception ex)
            {
                JavaScriptSerializer javaScriptSerializer = new JavaScriptSerializer();
                string txt = javaScriptSerializer.Serialize("failed");
                context.Response.ContentType = "text/html";
                context.Response.Write(txt);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}