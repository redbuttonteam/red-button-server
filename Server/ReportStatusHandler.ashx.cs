﻿using RedButton.Server.Entities;
using RedButton.Server.Helpers;
using RedButton.Server.SFClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace RedButton.Server
{
    /// <summary>
    /// Summary description for ReportStatusHandler
    /// </summary>
    public class ReportStatusHandler : IHttpHandler
    {
        private static ErrorHelper errorHelper;

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                errorHelper = ErrorHelper.Instance;
                try
                {
                    var jsonSerializer = new JavaScriptSerializer();
                    var jsonString = String.Empty;
                    context.Request.InputStream.Position = 0;
                    using (var inputStream = new StreamReader(context.Request.InputStream))
                    {
                        jsonString = inputStream.ReadToEnd();
                    }
                    if (jsonString != string.Empty)
                    {
                        List<ReportStatusRequest> requestedReports = jsonSerializer.Deserialize<List<ReportStatusRequest>>(jsonString);
                        if (requestedReports != null && requestedReports.Count > 0)
                        {
                            ResourceAccessHelper.ConnectToSalesForce();
                            List<RequestStatus> statuses = new List<RequestStatus>();
                            foreach (ReportStatusRequest report in requestedReports)
                            {
                                try
                                {
                                    RequestStatus status = GetCase(report.reportID, report.reportCode);
                                    if(status != null)
                                        statuses.Add(status);
                                }
                                catch (Exception)
                                {
                                    if (errorHelper == null)
                                    {
                                        errorHelper = ErrorHelper.Instance;
                                    }
                                    errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1007);
                                    return;
                                }
                            }
                            if (statuses != null && statuses.Count > 0)
                            {
                                context.Response.ContentType = "application/json";
                                context.Response.ContentEncoding = Encoding.UTF8;
                                context.Response.Write(jsonSerializer.Serialize(statuses));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    context.Response.Write(string.Format("{0}({1});", context.Request["callback"], new JavaScriptSerializer().Serialize(ex.Message)));
                    context.Response.ContentType = "application/json";
                }
            }
            catch (Exception)
            {
                if (errorHelper == null)
                {
                    errorHelper = ErrorHelper.Instance;
                }
                errorHelper.RaiseErrorToClient(context, 500, "server error", true, 1008);
                return;
            }
        }

        private RequestStatus GetCase(string caseNumber, string code)
        {
            var soapClient = ResourceAccessHelper.SoapClient;
            var sessionHeader = ResourceAccessHelper.SessionHeader;
            var mruHeader = new MruHeader { updateMru = true };
            var packageVersions = new PackageVersion[0];

            var statusQuery = "SELECT CaseNumber,CreatedDate, Status, PublicCaseStatus__c,URL__c, Name__c, Field1__c, Reason FROM Case " +
                              "WHERE CaseNumber = '" + caseNumber + "' and ReferenceViewCode__c = '" + code + "'";

            QueryResult result;

            soapClient.query(sessionHeader, new QueryOptions(), mruHeader, packageVersions, statusQuery, out result);
            if (result.records == null)
            {
                return null;
            }
            else
            {
                var singleCase = (Case)result.records[0];

                var pubStatus = singleCase.Status.Contains("טופל בהצלחה / הוסר") ||
                    singleCase.Status.Contains("נסגר") || singleCase.Status.Contains("נסגרה הבקשה") || singleCase.Status.Contains("הבקשה נסגרה") ?
                    singleCase.Reason : singleCase.PublicCaseStatus__c;

                var systemMessage = string.Empty;
                if (singleCase.Name__c != null)
                {
                    systemMessage = singleCase.Name__c;
                }

                DateTime timeToConvert = singleCase.CreatedDate.Value;
                TimeZoneInfo israelTime = TimeZoneInfo.FindSystemTimeZoneById("Israel Standard Time");
                DateTime targetTime = TimeZoneInfo.ConvertTime(timeToConvert, israelTime);
                string date = targetTime.ToString("dd/MM/yy HH:mm:ss");

                var status = new RequestStatus
                {
                    Date = date,
                    CaseNumber = singleCase.CaseNumber,
                    Status = singleCase.Status,
                    Url = singleCase.URL__c,
                    PubStatus = pubStatus,
                    SystemMessage = systemMessage
                };
                return status;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}