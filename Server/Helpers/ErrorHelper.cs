﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedButton.Server.Helpers
{
    public sealed class ErrorHelper
    {
        private static volatile ErrorHelper instance;
        private static object locker = new Object();

        private delegate void ErrorDelegate(HttpContext context,int code,
            string desc, bool skipIIS, int errorNumber);
        private event ErrorDelegate ErrorEvent;

        private ErrorHelper() { }

        public static ErrorHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (locker)
                    {
                        if (instance == null)
                        {
                            instance = new ErrorHelper();
                        }
                    }
                }
                return instance;
            }
        }

        public void RaiseErrorToClient(HttpContext context, int statusCode, string desc, bool skipIIS, int errorNumber)
        {
            if (ErrorEvent == null)
            {
                ErrorEvent += ErrorHelper_ErrorEvent;
            }
            ErrorEvent(context, statusCode, desc, skipIIS, errorNumber);
        }

        void ErrorHelper_ErrorEvent(HttpContext context, int statusCode, string desc, bool skipIIS, int errorNumber)
        {
            context.Response.Clear();
            context.Response.StatusCode = statusCode;
            context.Response.StatusDescription = desc;
            context.Response.TrySkipIisCustomErrors = skipIIS;
            context.Response.Write("Error Code: " + errorNumber);
            //context.Response.End();
            if (ErrorEvent != null)
            {
                ErrorEvent -= ErrorHelper_ErrorEvent;
            }
        }
    }
}