﻿using System.Web.Configuration;
using RedButton.Server.SFClient;

namespace RedButton.Server
{
    // Represents the communication with sales force web service
    public static class ResourceAccessHelper
    {
        private static string LoginUsername = WebConfigurationManager.AppSettings["SalesFroce_Username"];
        private static string LoginPassword = WebConfigurationManager.AppSettings["SalesFroce_Password"];

        private static SessionHeader _sessionHeader;
        private static SoapClient _soapClient;

        private static readonly AllOrNoneHeader _allOrNoneHeader = new AllOrNoneHeader { allOrNone = true };
        private static readonly AllowFieldTruncationHeader _allowFieldTruncationHeader = new AllowFieldTruncationHeader { allowFieldTruncation = false };
        private static readonly AssignmentRuleHeader _assignmentRuleHeader = new AssignmentRuleHeader { useDefaultRule = true };
        private static readonly DebuggingHeader _debuggingHeader = new DebuggingHeader { debugLevel = DebugLevel.DebugOnly };
        private static readonly DisableFeedTrackingHeader _disableFeedTrackingHeader = new DisableFeedTrackingHeader { disableFeedTracking = false };
        private static readonly EmailHeader _emailHeader = new EmailHeader { triggerAutoResponseEmail = true, triggerOtherEmail = true, triggerUserEmail = true };
        private static readonly StreamingEnabledHeader _streamingEnabledHeader = new StreamingEnabledHeader { streamingEnabled = true };
        private static readonly MruHeader _mruHeader = new MruHeader { updateMru = true };
        private static readonly PackageVersion[] _packageVersions = new PackageVersion[0];
        private static readonly LocaleOptions _localeOptions = new LocaleOptions { language = "en_US" };
        private static readonly DuplicateRuleHeader _duplicateRuleHeader = new DuplicateRuleHeader { allowSave = false, includeRecordDetails = true};

        // Getting the session header of the connection.
        public static SessionHeader SessionHeader
        {
            get
            {
                if (_sessionHeader == null)
                {
                    ConnectToSalesForce();
                }
                return _sessionHeader;
            }
        }

        // Getting the soap client of the connection.
        public static SoapClient SoapClient
        {
            get
            {
                if (_soapClient == null)
                {
                    ConnectToSalesForce();
                }

                return _soapClient;
            }
        }

        // Connecting to salesforce and initilizing a client.
        public static void ConnectToSalesForce()
        {
            // create client on start of application
            var loginClient = new SoapClient();
            var loginScopeHeader = new LoginScopeHeader();
            var res = loginClient.login(loginScopeHeader, LoginUsername, LoginPassword);
            _soapClient = new SoapClient("Soap", res.serverUrl);
            _sessionHeader = new SessionHeader { sessionId = res.sessionId };
        }

        // Uploading cases or attachments to salesforce.
        public static SaveResult[] CreateRequest(sObject[] objects)
        {
            SaveResult[] saveRes;
            LimitInfo[] limitInfo;
            _soapClient.create(_sessionHeader,
                               _assignmentRuleHeader,
                               _mruHeader,
                               _allowFieldTruncationHeader,
                               _disableFeedTrackingHeader,
                               _streamingEnabledHeader,
                               _allOrNoneHeader,
                               _duplicateRuleHeader,
                               _localeOptions,
                               _debuggingHeader,
                               _packageVersions,
                               _emailHeader,
                               objects,
                               out limitInfo,
                               out saveRes);
            return saveRes;
        }

        // Updating cases in salesforce.
        public static SaveResult[] UpdateRequest(sObject[] objects)
        {
            SaveResult[] saveRes;
            LimitInfo[] limitInfo;

            OwnerChangeOption opt1 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferContacts;

            OwnerChangeOption opt2 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferContracts;

            OwnerChangeOption opt3 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferNotesAndAttachments;

            OwnerChangeOption opt4 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferOpenActivities;

            OwnerChangeOption opt5 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferOrders;

            OwnerChangeOption opt6 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferOthersOpenOpportunities;

            OwnerChangeOption opt7 = new OwnerChangeOption();
            opt1.execute = true;
            opt1.type = OwnerChangeOptionType.TransferOwnedOpenOpportunities;

            OwnerChangeOption[] ownerOptions = { opt1, opt2, opt3, opt4, opt5, opt6, opt7 };
            _soapClient.update(_sessionHeader,
                               _assignmentRuleHeader,
                               _mruHeader,
                               _allowFieldTruncationHeader,
                               _disableFeedTrackingHeader,
                               _streamingEnabledHeader,
                               _allOrNoneHeader,
                               _duplicateRuleHeader,
                               _localeOptions,
                               _debuggingHeader,
                               _packageVersions,
                               _emailHeader,
                               ownerOptions,
                               objects,
                               out limitInfo,
                               out saveRes);
            return saveRes;
        }
    }
}