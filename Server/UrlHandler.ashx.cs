﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Web.Caching;
using System.Web.Script.Serialization;
using RedButton.Server.SFClient;

namespace RedButton.Server
{
    using System.Net;
    using System.Net.Mail;
    using System.Text;

    public class UrlHandler : IHttpHandler
    {
        public class EmailResponseObject
        {
            public bool success;
            public string message;
        }
        public class UrlsResponseObject
        {
            public bool success;
            public IList<string> urls;
        }

        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string response = string.Empty;
                string action = context.Request.QueryString["action"];
                string callback = context.Request.QueryString["callback"];
                switch (action)
                {
                    case "getUrlList":
                        response = GetUrlList(context);
                        break;
                    case "sendEmail":
                        var url = context.Request.QueryString["url"];
                        url = Uri.UnescapeDataString(url);
                        var email = context.Request.QueryString["email"];
                        response = SendEmail(url, email);
                        break;
                }

                if (!string.IsNullOrEmpty(callback))
                {
                    response = string.Format("{0}({1})", callback, response);
                }

                context.Response.Write(response);
                context.Response.ContentType = "application/json";
            }
            catch (Exception ex)
            {
                context.Response.StatusCode = 500;
                context.Response.Write(ex.Message);
            }
        }

        private string GetUrlList(HttpContext context)
        {
            IList<string> md5Collection;
            if (context.Cache.Get("urlList") == null)
            {
                ResourceAccessHelper.ConnectToSalesForce();

                var soapClient = ResourceAccessHelper.SoapClient;
                var sessionHeader = ResourceAccessHelper.SessionHeader;
                var mruHeader = new MruHeader { updateMru = true };
                var packageVersions = new PackageVersion[0];

                var urlQuery = "SELECT URL__c FROM Case WHERE URLFoundOffending__c=True";

                QueryResult qResult;
                soapClient.query(sessionHeader, new QueryOptions(), mruHeader, packageVersions, urlQuery, out qResult);

                md5Collection = (from result in qResult.records
                                 select CalculateMD5Hash(((Case)result).URL__c.ToLower())).ToList();
                context.Cache.Add("urlList", md5Collection, null, DateTime.Now.AddHours(0), Cache.NoSlidingExpiration, CacheItemPriority.Default, null);
            }
            else
            {
                md5Collection = (IList<string>)context.Cache.Get("urlList");
            }

            UrlsResponseObject responseObj = new UrlsResponseObject();
            responseObj.urls = md5Collection;
            responseObj.success = true;
            return new JavaScriptSerializer().Serialize(responseObj);

        }

        public string CalculateMD5Hash(string input)
        {

            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString().ToLower();
        }

        private string SendEmail(string url, string email)
        {
            EmailResponseObject responseObj = new EmailResponseObject();

            try
            {
                var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    Credentials = new NetworkCredential("redbuttonmailer@gmail.com", "redband1"),
                    EnableSsl = true
                };

                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("{0}<br/>", "שלום,");
                sb.AppendFormat("{0}<br/>", "מחשבך ביצע גישה לעמוד שדווח כפוגעני.");
                sb.AppendFormat("{0}{1}<br/>", "כתובת העמוד : ", url);
                sb.AppendFormat("{0}<br/>", "בברכה,");
                sb.AppendFormat("{0}<br/>", "צוות הכפתור האדום");
                var from = new MailAddress("redbuttonmailer@gmail.com");
                var to = new MailAddress(email);
                MailMessage m = new MailMessage(from, to);
                m.IsBodyHtml = true;
                m.Body = sb.ToString();

                m.Subject = "הכפתור האדום - דיווח עמוד פוגעני ";
                client.Send(m);
                responseObj.success = true;
            }
            catch (Exception)
            {
                responseObj.success = false;
            }


            return new JavaScriptSerializer().Serialize(responseObj);
        }




        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}