﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedButton.Server.Entities
{
    public class RequestReport
    {
        public string userComment { set; get; }
        public string code { set; get; }
        public Uri uri { set; get; }
        public byte[] attachmentBody { set; get; }
        public string phoneNumber { get; set; }
        public string nameInfo { get; set; }
        public string contentSource { get; set; }
        public string eventType { get; set; }
        public string userID { get; set; }
        public string timeUpload { get; set; }
        public string pageName { get; set; }
        public string appName { get; set; }
        public bool isSuicidalThreat { get; set; }
        public string distressCaseDescription { get; set; }
         
    }
}