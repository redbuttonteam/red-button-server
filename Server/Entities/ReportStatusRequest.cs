﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RedButton.Server
{
    public class ReportStatusRequest
    {
        public string reportID { get; set; }
        public string reportCode { get; set; }
    }
}