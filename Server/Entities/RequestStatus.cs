﻿namespace RedButton.Server.Entities
{
    public class RequestStatus
    {
        public string Date { get; set; }

        public string CaseNumber { get; set; }

        public string Status { get; set; }

        public string PubStatus { get; set; }

        public string Body { get; set; }

        public int? BodyLength { get; set; }

        public string ContentType { get; set; }

        public string Url { get; set; }

        public string SystemMessage { get; set; }
    }
}